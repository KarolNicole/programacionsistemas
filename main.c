#include <point.h>

int main()
{
	point a;
	point b;
	printf("CALCULO DE DISTANCIA EUCLIDIANA ENTRE DOS PUNTOS\n");
	printf("*** PRIMER PUNTO(X,Y;Z) ***\n");
	printf("Ingrese x: ");
	scanf("%f",&a.x);
	getchar();
	printf("Ingrese y: ");
	scanf("%f",&a.y);
	getchar();	
	printf("Ingrese z: ");
	scanf("%f",&a.z);
	getchar();
	printf("*** SEGUNDO PUNTO(X,Y;Z) ***\n");
	printf("Ingrese x: ");
	scanf("%f",&b.x);
	getchar();
	printf("Ingrese y: ");
	scanf("%f",&b.y);
	getchar();
	printf("Ingrese z: ");
	scanf("%f",&b.z);
	getchar();
	float dist= distancia(a,b);
	printf("La distancia entre los dos puntos es: %f",dist);
	getchar();		
	printf("CALCULO DE PUNTO MEDIO \n");
	point pmedio= puntomedio(a,b);
	printf("Punto medio es: ( %f , %f , %f )",pmedio.x,pmedio.y,pmedio.z);
	
}
