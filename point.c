#include <point.h>
#include <math.h>

float x,y,z,d,suma;
point res;
float distancia(point a, point b)
{
	x= a.x-b.x;
	x= pow(x,2);
	y= a.y-b.y;
	y= pow(y,2);
	z= a.z-b.z;
	z= pow(z,2);
	suma= x+y+z;
	d= sqrt(suma); 
	return d;
}

point puntomedio(point a, point b)
{
	res.x = (a.x + b.x) / 2;
	res.y = (a.y + b.y) / 2;
	res.z = (a.z + b.z) / 2;
	return res;

}
