CC=gcc
idir= ./includes
CFLAGS=-c -I$(idir) -lm

all: point

point: main.o point.o
	$(CC) -o point main.o point.o -I$(idir) -lm

main.o: main.c
	$(CC) $(CFLAGS) main.c

point.o: point.c $(idir)/point.h
	$(CC) $(CFLAGS) point.c

clean:
	rm *o point
